part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  Routes._();
  static const HOME = _Paths.HOME;
  static const LOGIN = _Paths.LOGIN;
  static const LOGIN2 = _Paths.LOGIN2;
  static const RESULT = _Paths.RESULT;
  static const QUIZ_RESULT = _Paths.QUIZ_RESULT;
}

abstract class _Paths {
  _Paths._();
  static const HOME = '/home';
  static const LOGIN = '/login';
  static const LOGIN2 = '/login2';
  static const RESULT = '/result';
  static const QUIZ_RESULT = '/quiz-result';
}
