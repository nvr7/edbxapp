import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:otp_text_field/otp_field.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:dio/dio.dart';
import 'package:app3/app/modules/login/models/user_model.dart' as modelUser;
import 'package:app3/app/modules/login/models/login_model.dart' as modelLogin;
import 'package:app3/app/modules/home/controllers/home_controller.dart';

class LoginController extends GetxController {
  final Dio dio = Dio();
  final HomeController homeController = Get.find<HomeController>();
  final FirebaseAuth _auth = FirebaseAuth.instance;
  String verificationIdReceived = '';
  RxString textFieldValue = ''.obs;
  RxString otpFieldValue = ''.obs;
  TextEditingController numController = TextEditingController();
  OtpFieldController otpController = OtpFieldController();
  bool otpSent = false;
  bool accountExistence = false;
  String uname = '';
  String pass = '';
  String parentId = '';
  String authTokens = 'eyJhbGciOiJIUzI1NiIsInR5cCI6ImFjY2VzcyJ9.eyJpYXQiOjE2ODgzNTQ1MDEsImV4cCI6MTY5MDk0NjUwMSwiYXVkIjoiaHR0cHM6Ly9lZGJ4LnBpbmlzaS5pbyIsImlzcyI6ImV4cHJlc3MiLCJzdWIiOiI2MTNmOTQ4MjczN2JhZjdhNDM4MjEyNTciLCJqdGkiOiIxMDI2ZmNhYy0zNjBjLTQyOGMtOTA2ZC03OTZmMDUwYmUxNGMifQ.81pXqOaRMMMWXRjA2P0XFXFFHcokal38eSBNP_MDS8U';

  void updateText(String value) {
    textFieldValue.value = value;
    print(textFieldValue.toString());
  }

  void otpUpdate(String value) {
    otpFieldValue.value = value;
    print(otpFieldValue.toString());
  }

  Future<void> sendSms() async {
    String value = textFieldValue.value;
    await _auth.verifyPhoneNumber(
      phoneNumber: '+62$value',
      verificationCompleted: (PhoneAuthCredential credential) async {
        await _auth.signInWithCredential(credential).then((value) {
          print('auth...');
        });
      },
      verificationFailed: (FirebaseAuthException e) {
        print('Verification Failed: ${e.code} - ${e.message}');
      },
      codeSent: (String verificationId, int? resendToken) {
        verificationIdReceived = verificationId;
        otpSent = true;
        print('Verification ID: $verificationId');
        print('OTP set to true');
      },
      codeAutoRetrievalTimeout: (String verificationID) {},
    );

    print('SMS sent to: $value');
    textFieldValue.value = '';
  }

  Future<void> createAccountInApi() async {
  try {
    print('check account existence');
    await checkAccountExistence();
    print(accountExistence);

    if (accountExistence != false) {
      print('Account exist');
      loginUserApi();
      return;
    }
    print('Account not found, create new account');

    dio.options.headers['Content-Type'] = 'application/json';

    final user = _auth.currentUser;
    if (user != null) {
      final phoneNumber = (_auth.currentUser?.phoneNumber ?? '').replaceFirst('+', '');
      final email = '${phoneNumber.replaceFirst('62', '')}@gmail.com';
      final url = 'https://edbx.pinisi.io/users';

      final body = modelUser.User(
        username: phoneNumber,
        email: email,
        password: '19230712',
        phone: phoneNumber
      );

      final jsonBody = jsonEncode(body.toJson());
      print('Account creation request body: $jsonBody');

      final response = await dio.post(url, data: body);
      if (response.statusCode == 200 | 201) {
        print('Account created in API');
        print(response.statusCode.toString());
        homeController.studentData();
        Get.toNamed('/home');
      } else {
        print('Failed to create account in API');
        print(response.statusCode.toString());
      }
    }
  } catch (e) {
    print('Error creating account in API: $e');
  }
}

  void clearTextField() {
    numController.clear();
  }

  void clearOtpField() {
    otpController.clear();
  }

  void login() async {
    String otpValue = otpFieldValue.value;
    print('OTP auth: $otpValue');
    print(verificationIdReceived);
    if (otpSent) {
      PhoneAuthCredential credential = PhoneAuthProvider.credential(
        verificationId: verificationIdReceived,
        smsCode: otpValue,
      );
      await _auth.signInWithCredential(credential).then((value) async {
        print('account?');
        await createAccountInApi();
      });
    }
  }

  Future<void> loginUserApi() async {
    try {
      final dio = Dio();
      final phoneNumber = (_auth.currentUser?.phoneNumber ?? '').replaceFirst('+', '');
      dio.options.headers['Content-Type'] = 'application/json';
      final loginUrl = 'https://edbx.pinisi.io/authentication';
      final body = modelLogin.Login(
        username: phoneNumber,
        password: '19230712',
      ); 
      final jsonBody = jsonEncode(body.toJson());
      print('Account Login request body: $jsonBody');

      final response = await dio.post(loginUrl, data: body);
      if (response.statusCode == 200 | 201) {
        print('Login Successed');
        print(response.statusCode.toString());
        homeController.studentData();
        Get.toNamed('/home');
      } else {
        print('Login Failed');
        print(response.statusCode.toString());
      }
    } catch(e){
      print('Login error : $e');
    }
  }

  Future<void> checkAccountExistence() async {
    dio.options.headers['Content-Type'] = 'application/json';

    final phoneNumber = (_auth.currentUser?.phoneNumber ?? '').replaceFirst('+', '');
    print('Check accouont with username : $phoneNumber');
    final accountCheckUrl = 'https://edbx.pinisi.io/users?username=$phoneNumber';
    dio.options.headers['Authorization'] = 'Bearer $authTokens';

    final accountCheckResponse = await dio.get(accountCheckUrl);
    if (accountCheckResponse.statusCode == 200) {
      final accountData = accountCheckResponse.data;
      if (accountData != null && accountData['data'] is List && accountData['data'].isNotEmpty) {
        accountExistence = true;
        print(accountExistence);
        final data = accountData['data'][0];
        parentId = data['_id'];
        homeController.parentIds(parentId);
      } else {
        print('Account not found');
      }
    }
  }
}
