import 'package:dio/dio.dart';
import 'package:get/get.dart';

class QuizResultController extends GetxController {
  final Dio dio = Dio();
  final score = 0.0.obs;
  final jsonData = <String, dynamic>{}.obs;
  var quizName = ''.obs;
  String authTokens = 'eyJhbGciOiJIUzI1NiIsInR5cCI6ImFjY2VzcyJ9.eyJpYXQiOjE2ODgzNTQ1MDEsImV4cCI6MTY5MDk0NjUwMSwiYXVkIjoiaHR0cHM6Ly9lZGJ4LnBpbmlzaS5pbyIsImlzcyI6ImV4cHJlc3MiLCJzdWIiOiI2MTNmOTQ4MjczN2JhZjdhNDM4MjEyNTciLCJqdGkiOiIxMDI2ZmNhYy0zNjBjLTQyOGMtOTA2ZC03OTZmMDUwYmUxNGMifQ.81pXqOaRMMMWXRjA2P0XFXFFHcokal38eSBNP_MDS8U';

  Future<void> resultQuiz() async {
    print('quizResult called');
    try {
      quizData();
      dio.options.headers['Content-Type'] = 'application/json';
      dio.options.headers['Authorization'] = 'Bearer $authTokens';
      final url = 'https://edbx.pinisi.io/quiz-results?quizId=60de806472ab7044896c619d&studentId=60daaced8107c72c512d7734';

      final resultQuizResponse = await dio.get(url);
      if (resultQuizResponse.statusCode == 200) {
        final responseData = resultQuizResponse.data;
        print('API request successful. Response received.');
        print('Response Raw : $responseData');

        jsonData.value = responseData;

        final scoreData = responseData['data'][0]['score'];
        score.value = double.parse(scoreData.toString());
        print(score.value);
      } else {
        print('API request failed. Status code: ${resultQuizResponse.statusCode}');
      }
    } catch (e) {
      print('Error retrieving Quiz data $e');
    }
  }
  
  Future<void> quizData() async {
    dio.options.headers['Content-Type'] = 'application/json';
    dio.options.headers['Authorization'] = 'Bearer $authTokens';
    final quizDataUrl = 'https://edbx.pinisi.io/quizzes?_id=60de806472ab7044896c619d';

    final quizDataResponse = await dio.get(quizDataUrl);
    if (quizDataResponse.statusCode == 200){
      final responseData = quizDataResponse.data;
      print('Raw Response : $responseData');

      final quizData = responseData['data'][0]['title'];
      quizName.value = quizData;
      print('quiz name : $quizName');

    }
  }
}
