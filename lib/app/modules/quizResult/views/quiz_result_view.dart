import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/quiz_result_controller.dart';

class QuizResultView extends GetView<QuizResultController> {
  const QuizResultView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);
    return Scaffold(
      backgroundColor: Color.fromARGB(225, 255, 255, 255),
      body: _resultBody(mediaQuery)
    );
  }

  Widget _resultBody(MediaQueryData mediaQuery){
    return LayoutBuilder(
      builder: (context, constraints) {
        return SingleChildScrollView(
          child: Column(
            children: [
              _buildHeader(mediaQuery),
              SizedBox(
                height: mediaQuery.size.height * 0.05,
              ),
              _buildScore(mediaQuery)
            ],
          ),
        );
      },
    );
  }

  Widget _buildHeader(MediaQueryData mediaQuery) {
    return Container(
      height: mediaQuery.size.height * 0.12,
      padding: EdgeInsets.all(16),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(25),
          bottomRight: Radius.circular(25)
        ),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.shade600,
            offset: Offset(0, 4),
            blurRadius: mediaQuery.size.width * 0.08,
          ),
        ],
        color: Color.fromARGB(255, 2, 62, 2)
      ),
      child: _buildHeaderNav(mediaQuery),
    );
  }

  Widget _buildHeaderNav(MediaQueryData mediaQuery){
    return Container(
      padding: EdgeInsets.only(top: mediaQuery.size.height * 0.02),
      child: Row(
        children: [
          IconButton(
            onPressed: () {
              Get.back();
            }, 
            icon: Icon(Icons.arrow_back)
          ),
          Text(
            'Nilai Ujian',
            style: TextStyle(
              // fontWeight: FontWeight.bold,
              fontSize: mediaQuery.size.width * 0.045
            ),
          )
        ],
      ),
    );
  }

  Widget _buildScore(MediaQueryData mediaQuery) {
    return Obx(() {
      final scoreValue = controller.score.value;
      final quizName = controller.quizName.value;
      return Container(
        width: mediaQuery.size.width * 0.8,
        height: mediaQuery.size.height * 0.5,
        decoration: BoxDecoration(
          color: Color.fromARGB(0, 255, 255, 255)
        ),
        child: Text(
          '$quizName: $scoreValue',
          style: TextStyle(
            color: Colors.black,
            fontSize: 20,
            // fontWeight: FontWeight.bold,
          ),
        ),
      );
    });
  }
}
