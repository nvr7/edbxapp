import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class Login2Controller extends GetxController {
  //TODO: Implement Login2Controller
  RxString unameFieldValue = ''.obs;
  RxString passFieldValue = ''.obs;
  TextEditingController unameController = TextEditingController();
  TextEditingController passController = TextEditingController();
  // OtpFieldController otpController = OtpFieldController();

  void unameUpdate(String unameValue) {
    unameFieldValue.value = unameValue;
    print(unameFieldValue.toString());
  }

  void passUpdate(String passValue) {
    passFieldValue.value = passValue;
    print(passFieldValue.toString());
  }

  void login2(){
    loginUserApi();
    // Get.toNamed('/home');
  }

  Future<void> loginUserApi() async {
    try{
      final dio = Dio();
      final uname = unameFieldValue.value;
      final pass = passFieldValue.value;
      dio.options.headers['Content-Type'] = 'application/json';

      final url = 'https://edbx.pinisi.io/authentication';
      final body = jsonEncode({
        "strategy": "local",
        "username": uname,
        "password": pass
      });

      print('Account login request body : $body');
      final response = await dio.post(url, data: body);
      if (response.statusCode == 200 | 201){
        print(response.statusCode.toString());
        print('User login');
        Get.toNamed('/home');
      } else {
        print('Failed login');
        print(response.statusCode.toString());
      }
    } catch (e){
      print('Error login in API: $e');
    }
  }

  void clearUnameField() {
    unameController.clear();
  }
  void clearPassField() {
    passController.clear();
  }
}
