import 'package:get/get.dart';

import '../controllers/login2_controller.dart';

class Login2Binding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<Login2Controller>(
      () => Login2Controller(),
    );
  }
}
