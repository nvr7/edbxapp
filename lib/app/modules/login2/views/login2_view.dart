import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../controllers/login2_controller.dart';

class Login2View extends GetView<Login2Controller> {
  // const Login2View({Key? key}) : super(key: key);
  final Login2Controller controller = Get.put(Login2Controller());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 2, 65, 2),
      body: Container(
        height: MediaQuery.of(context).size.height, // Set the container height to match the screen height
        child: ListView(
          children: [
            SingleChildScrollView(
              child: Column(
                children: [
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.05,
                  ),
                  Center(
                    child: CircleAvatar(
                      backgroundColor: Colors.white,
                      radius: MediaQuery.of(context).size.height * 0.08,
                    ),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.02,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(
                      horizontal: MediaQuery.of(context).size.width * 0.1,
                      vertical: MediaQuery.of(context).size.height * 0.1,
                    ),
                    child: Column(
                      children: [
                        Container(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            'Login',
                            style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        // SizedBox(
                        //   height: MediaQuery.of(context).size.height * 0.1,
                        // ),
                        TextField(
                          controller: controller.unameController,
                          onChanged: (value) => controller.unameUpdate(value),
                          decoration: InputDecoration(
                            labelText: 'username',
                          ),
                          // keyboardType: TextInputType.phone,
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.height * 0.05,
                        ),
                        TextField(
                          controller: controller.passController,
                          onChanged: (value) => controller.passUpdate(value),
                          decoration: InputDecoration(
                            labelText: 'password',
                          ),
                          // keyboardType: TextInputType.phone,
                        ),
                        
                        SizedBox(
                          height: MediaQuery.of(context).size.height * 0.1,
                        ),
                        SizedBox(
                          width: MediaQuery.of(context).size.width * 0.5,
                          height: MediaQuery.of(context).size.height * 0.05,
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              elevation: 0.0,
                              backgroundColor: Colors.white,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(
                                  MediaQuery.of(context)
                                          .size
                                          .height *
                                      0.06 *
                                      0.5,
                                ),
                              ),
                            ),
                            child: const Text(
                              'Login',
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 16,
                              ),
                            ),
                            onPressed: () {
                              // if (controller.otpSent=true){
                              //   controller.login();
                              //   controller.clearOtpField();
                              // } else {

                              // }
                              controller.login2();
                              controller.clearUnameField();
                              controller.clearPassField();
                            },
                            // onPressed: () => controller.login(),
                          ),
                        ),
                        SizedBox(
                          height: 4,
                        ),
                        TextButton(
                          child: Text(
                            'Register'
                          ),
                          onPressed:() {
                            Get.toNamed('/login');
                          },
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
