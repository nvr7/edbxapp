import 'package:app3/app/modules/quizResult/controllers/quiz_result_controller.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/result_controller.dart';

class ResultView extends GetView<ResultController> {
  // const ResultView({Key? key}) : super(key: key);
  final QuizResultController quizResultController = Get.put(QuizResultController());
  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);
    return Scaffold(
      backgroundColor: Color.fromARGB(225, 255, 255, 255),
      body: _resultBody(mediaQuery)
    );
  }

  Widget _resultBody(MediaQueryData mediaQuery){
    return LayoutBuilder(
      builder: (context, constraints) {
        return SingleChildScrollView(
          child: Column(
            children: [
              _buildHeader(mediaQuery),
              _buildGridView(mediaQuery)
            ],
          ),
        );
      },
    );
  }

  Widget _buildHeader(MediaQueryData mediaQuery) {
    return Container(
      height: mediaQuery.size.height * 0.12,
      padding: EdgeInsets.all(8),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(mediaQuery.size.width * 0.06),
          bottomRight: Radius.circular(mediaQuery.size.width * 0.06)
        ),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.shade600,
            offset: Offset(0, 4),
            blurRadius: 10,
          ),
        ],
        color: Color.fromARGB(255, 2, 62, 2)
      ),
      child: _buildHeaderNav(mediaQuery),
    );
  }

  Widget _buildHeaderNav(MediaQueryData mediaQuery){
    return Container(
      padding: EdgeInsets.only(top: mediaQuery.size.height * 0.02),
      child: Row(
        children: [
          IconButton(
            onPressed: () {
              Get.back();
            }, 
            icon: Icon(Icons.arrow_back)
          ),
          Text(
            'Nilai Ujian',
            style: TextStyle(
              // fontWeight: FontWeight.bold,
              fontSize: mediaQuery.size.width * 0.045
            ),
          )
        ],
      ),
    );
  }

  Widget _buildGridView(MediaQueryData mediaQueary){
    return GridView.builder(
      padding: EdgeInsets.all(mediaQueary.size.width * 0.06),
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        crossAxisSpacing: mediaQueary.size.width * 0.08,
        mainAxisSpacing: mediaQueary.size.width * 0.08
      ),
      itemCount: 4,
      itemBuilder: (context, index) {
        List<String> itemDesc = [
          'Ujian Tengah Semester',
          'Ujian Akhir Semester',
          'Quiz',
          'Ulangan'
        ];

        List<IconData> listIcon =[
          Icons.assignment,
          Icons.assignment,
          Icons.assignment,
          Icons.assignment,
        ];

        return GestureDetector(
          onTap: () {
            // Handle the box press event
            if (index == 0) {
              //page UTS
            } else if (index == 1) {
              //page UAS
            } else if (index == 2) {
              //page Quiz
              quizResultController.resultQuiz();
              Get.toNamed('/quiz-result');
            } else if (index == 3) {
              //page Ulangan
            }
          },
          child: Container(
          key: ValueKey('item_$index'),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(mediaQueary.size.width * 0.06),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.shade600,
                offset: Offset(0, 4),
                blurRadius: mediaQueary.size.width * 0.03,
              ),
            ],
          ),
          child: Column(
            children: [
              Expanded(
                child: Align(
                  alignment: Alignment.center,
                  child: Icon(
                    listIcon[index],
                    color: Colors.black,
                    size: mediaQueary.size.width * 0.18,
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(
                  bottom: mediaQueary.size.height * 0.02,
                  left: mediaQueary.size.width * 0.025
                ),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    itemDesc[index],
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: mediaQueary.size.width * 0.035,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ));
      },
    );
  }
}
